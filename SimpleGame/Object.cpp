#include "stdafx.h"
#include "Object.h"
#include "math.h"
#include <windows.h>
#include <float.h>
#include <iostream>
#include "global.h"


Object::Object()
{
	float m_bulletcoolTime = 0.0f;
	float m_bulletTime = 0.2f;
}


Object::~Object()
{

}

void Object::InitBulletCooltime()
{
	m_bulletcoolTime = m_bulletTime;


}

bool Object::CanFireBullet()
{
	if (m_bulletcoolTime <= 0.f)
	{
		return true;
	}
	return false;
}


void Object::SetPos(float x, float y, float z)
{
	m_PosX = x; 
	m_PosY = y; 
	m_PosZ = z;
}

void Object::GetPos(float *x, float *y, float *z)
{
	*x = m_PosX;
	*y = m_PosY;
	*z = m_PosZ;
}
void Object::SetSize(float sX, float sY, float sZ)
{
	m_sX = sX;
	m_sY = sY;
	m_sZ = sZ;
}

void Object::GetSize(float *sX, float *sY, float *sZ)
{
	*sX = m_sX;
	*sY = m_sY;
	*sZ = m_sZ;
}

void Object::SetVel(float x, float y, float z)
{
	m_VelX = x;
	m_VelY = y;
	m_VelZ = z;
}

void Object::GetVel(float *x, float *y, float *z)
{
	*x = m_VelX;
	*y = m_VelY;
	*z = m_VelZ;
}

void Object::SetAcc(float x, float y, float z)
{
	m_AccX = x;
	m_AccY = y;
	m_AccZ = z;
}

void Object::GetAcc(float *x, float *y, float *z)
{
	*x = m_AccX;
	*y = m_AccY;
	*z = m_AccZ;
}

void Object::SetColor(float R, float G, float B, float A)
{

	m_R = R;
	m_G = G;
	m_B = B;
	m_A = A;
}

void Object::GetColor(float *R, float *G, float *B, float *A)
{
	*R = m_R;
	*G = m_G;
	*B = m_B;
	*A = m_A;

}

void Object::SetMass(float mass)
{
	m_Mass = mass;
}
void Object::GetMass(float * mass)
{
	*mass = m_Mass;
}


void Object::Update(float elapsed_Time)    
{
	m_bulletcoolTime -= elapsed_Time;
	if (m_bulletcoolTime <= 0.f) 
	{ 
		m_bulletcoolTime = -1.f; 
	}
	
	float velocity = sqrtf((m_VelX * m_VelX) + (m_VelY * m_VelY)+ (m_VelZ * m_VelZ)); 
	if (velocity < FLT_EPSILON) 
	{
		m_VelX = 0.f;
		m_VelY = 0.f;
		m_VelZ = 0.f;
	}
	else				
	{

			float gZ;
			gZ = GRAVITY * m_Mass;      
			float friction;
			friction = m_FrictionCoef * gZ;

			
			float fX, fY;
			fX = -friction * m_VelX / velocity;
			fY = -friction * m_VelY / velocity;
			
			float accX, accY;
			accX = fX / m_Mass;
			accY = fY / m_Mass;

			float afterVelX = m_VelX + elapsed_Time * accX;
			float afterVelY = m_VelY + elapsed_Time * accY;

			if (afterVelX*m_VelX < 0.f)
			{
				m_VelX = 0.f;

			}
			else
				m_VelX = m_VelX + elapsed_Time * accX;

			if (afterVelY*m_VelY < 0.f)
			{
				m_VelY = 0.f;

			}
			else
				m_VelY = m_VelY + elapsed_Time * accY;
	//	}
	
		if (m_state == STATE_AIR)
		{
			m_VelZ = m_VelZ + elapsed_Time * (-GRAVITY);
		}

	
	}
	m_VelX = m_VelX + elapsed_Time * m_AccX;
	m_VelY = m_VelY + elapsed_Time * m_AccY;
	m_VelZ = m_VelZ + elapsed_Time * m_AccZ;
	
	m_PosX = m_PosX + elapsed_Time * m_VelX;  
	m_PosY = m_PosY + elapsed_Time * m_VelY;
	m_PosZ = m_PosZ + elapsed_Time * m_VelZ;


	if (m_PosZ > 0.f)
	{
		m_state = STATE_AIR;
	}
	else
	{
		m_state = STATE_GROUND;
		m_PosZ = 0.f;
		m_VelZ = 0.f;

	}

}


void Object::Addforce(float _x, float _y, float _z, float _etime)
{
		//calc acc
		m_AccX =  _x/ m_Mass;
		m_AccY =  _y/ m_Mass;
		m_AccZ =  _z/ m_Mass;

		m_VelX = m_VelX + m_AccX * _etime;
		m_VelY = m_VelY + m_AccY * _etime;
		m_VelZ = m_VelZ + m_AccZ * (0.1f);

		m_AccX = 0.f;
		m_AccY = 0.f;
		m_AccZ = 0.f;

}

void Object::SetFrictionCoef(float x)
{
	m_FrictionCoef = x;
}

void Object::GetFrictionCoef(float *x)
{
	*x = m_FrictionCoef;
}

void Object::SetHP(int hp)
{
	m_HP = hp;

}

void Object::GetHP(int *hp)
{
	*hp = m_HP;


}

void Object::SetKind(int kind)
{
	m_kind = kind;

}


void Object::GetKind(int *kind)
{
	*kind = m_kind;

}

void Object::SetState(int state)
{
	m_state = state;

}
void Object::GetState(int *state)
{
	*state = m_state;

}

