/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "ScnMgr.h"

using namespace std;

bool g_KeyW = false;
bool g_KeyA = false;
bool g_KeyS = false;
bool g_KeyD = false;
bool g_KeySP = false;
ScnMgr *g_ScnMgr = nullptr;
DWORD g_PrevTime = 0;
int g_Shoot = 0;
int screen_width = 1000;
int screen_height = 700;

void RenderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.4f, 0.2f, 0.f, 1.f);   
	if (g_PrevTime == 0)
	{
		g_PrevTime = timeGetTime();  
	}
	DWORD currTime = timeGetTime();			
	DWORD elapsedTime = currTime - g_PrevTime;	 
	g_PrevTime = currTime;		
	float eTime = (float)(elapsedTime / 1000.f);   
	
	float fx = 0.f, fy = 0.f, fz = 0.f;
	float amount = 9.f;
	float jump_amount = 50.f;
	
	if (g_KeyW) {
		fy += amount;
	}
	if (g_KeyA) {
		fx -= amount;
	}
	if (g_KeyS) {
		fy -= amount;
	}
	if (g_KeyD) {
		fx += amount;
	}
	if (g_KeySP) {
		fz += jump_amount;
	}
	

	g_ScnMgr->Addforce(fx, fy, fz, eTime);
	g_ScnMgr->Update(eTime);			
	g_ScnMgr->RenderScene();				
	g_ScnMgr->Shoot(g_Shoot, eTime);			
		
	g_ScnMgr->UpdateCollision();
	g_ScnMgr->DoGarbageCollect();
	
	glutSwapBuffers();

}

void Idle(void)
{
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}


void KeyDownInput(unsigned char key, int x, int y)  
{
	if (key == 'w')
	{
		g_KeyW = true;													

	}

	if (key == 'a')
	{
		g_KeyA = true;
		
	}

	if (key == 's')
	{
		g_KeyS = true;

	}

	if (key == 'd')
	{
		g_KeyD = true;


	}
	
	if (key == ' ')
	{
      	g_KeySP = true;

	}
	RenderScene();
														

}


void KeyUpInput(unsigned char key, int x, int y)	
{
	if (key == 'w')
	{
		g_KeyW = false;

	}

	if (key == 'a')
	{
		g_KeyA = false;
	}


	if (key == 's')
	{
		g_KeyS = false;
	}

	if (key == 'd')
	{
		g_KeyD = false;
	}
	
	if (key == ' ')
	{
		g_KeySP = false;
	}
	
	RenderScene();

}


void SpecialKeyInput(int key, int x, int y)
{
	RenderScene();
}

void SpecialKeyDownInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_Shoot = SHOOT_UP;
		
		break;
	case GLUT_KEY_DOWN:
		g_Shoot = SHOOT_DOWN;
		
		break;
	case GLUT_KEY_RIGHT:
		g_Shoot = SHOOT_RIGHT;
		
		break;
	case GLUT_KEY_LEFT:
		g_Shoot = SHOOT_LEFT;
		break;
	}
	RenderScene();
	
}
void SpecialKeyUpInput(int key, int x, int y)
{
	g_Shoot = SHOOT_NONE;
	RenderScene();
}


int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(screen_width, screen_height);
	glutCreateWindow("Game Software Engineering KPU");



	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);
	glutKeyboardUpFunc(KeyUpInput);
	//glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);	
	g_ScnMgr = new ScnMgr(screen_width, screen_height);

	glutMainLoop();

	delete g_ScnMgr;
	
    return 0;
}

