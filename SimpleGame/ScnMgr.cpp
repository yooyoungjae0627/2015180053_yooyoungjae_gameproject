#pragma once

#include "Object.h"
#include "Renderer.h"
#include "ScnMgr.h"
#include <Windows.h>
#include "global.h"
#include <iostream>

int currSeqX = 0;
int currSeqY = 0;
int currSeqX_Count = 0;

ScnMgr::ScnMgr(int width, int height)
{
	m_screen_width = width;
	m_screen_height = height;

	
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
		
	}
	
	m_Objects[HERO_ID] = new Object();    
	m_Objects[HERO_ID]->SetPos(-3.5f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetVel(0.f, 0.f, 0.f);
	m_Objects[HERO_ID]->SetAcc(0.f, 0.f, 0.f);  
	m_Objects[HERO_ID]->SetMass(1.f);
	m_Objects[HERO_ID]->SetFrictionCoef(0.5f);
	m_Objects[HERO_ID]->SetSize(1.f, 1.f, 0.f);	
	m_Objects[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f); 
	m_Objects[HERO_ID]->SetKind(HERO_ID);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);
	m_Objects[HERO_ID]->SetHP(3000);

	AddObject(-4.7f, -1.75f, 0.f, 
		0.f, 0.f, 0.f, 
		0.5f, 6.7f, 99.f, KIND_LEFT_WALL, 1000.f, STATE_GROUND);
	AddObject(4.7f, -1.75f, 0.f,
		0.f, 0.f, 0.f,
		0.5f, 6.7f, 99.f, KIND_RIGHT_WALL, 1000.f, STATE_GROUND);
	AddObject(0.f, 2.8f, 0.f,
		0.f, 0.f, 0.f,
		10.f, 0.5f, 99.f, KIND_UP_WALL, 1000.f, STATE_GROUND);
	AddObject(0.f, -3.3f, 0.f,
		0.f, 0.f, 0.f,
		8.87f, 0.5f, 99.f, KIND_DOWN_WALL, 1000.f, STATE_GROUND);
	AddObject(2.2f, 0.4f, 0.f,
		0.f, 0.f, 0.f,
		1.f, 1.f, 0.5f, KIND_ENEMY, 40000.f, STATE_GROUND);
	AddObject(-1.9f, 1.5f, 0.f,
		0.f, 0.f, 0.f,
		1.f, 1.f, 0.5f, KIND_ENEMY, 40000.f, STATE_GROUND);
	AddObject(-3.1f, -1.5f, 0.f,
		0.f, 0.f, 0.f,
		1.f, 1.f, 0.5f, KIND_ENEMY, 40000.f, STATE_GROUND);
	AddObject(3.8f, -2.3f, 0.f,
		0.f, 0.f, 0.f,
		1.f, 1.f, 0.5f, KIND_ENEMY, 40000.f, STATE_GROUND);

	m_Renderer = new Renderer(width, height);  
	m_TextIsaac = m_Renderer->CreatePngTexture("./Textures/yyj_char.png");	
	m_TexLWall = m_Renderer->CreatePngTexture("./Textures/block_v.png");
	m_TexBullet = m_Renderer->CreatePngTexture("./Textures/bead.png");
	m_TexUWall = m_Renderer->CreatePngTexture("./Textures/block_h.png");
	m_TexEnemy = m_Renderer->CreatePngTexture("./Textures/many_eyes.png");

	m_sound = new Sound();
	m_soundBG = m_sound->CreateSound("./Sounds/desert.mp3");
	m_sound->PlaySounds(m_soundBG, true, 3.f);
	m_soundFiring = m_sound->CreateSound("./Sounds/gun_sound.wav");
	
}


ScnMgr::~ScnMgr()     
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		DeleteObject(i);
		
	}
	
	m_Renderer->DeleteTexture(m_TextIsaac);
	delete m_Renderer;
	m_Renderer = NULL;

}


void ScnMgr::RenderScene() 
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;
		float x, y, z;
		m_Objects[i]->GetPos(&x, &y, &z);     
		float sX, sY, sZ;
		m_Objects[i]->GetSize(&sX, &sY, &sZ);
		float r, g, b, a;
		m_Objects[i]->GetColor(&r, &g, &b, &a);
		int hp;
		m_Objects[i]->GetHP(&hp);
		float newX, newY, newZ, newsX, newsY, newsZ, newhp;		
		newX = x * 100.f;
		newY = y * 100.f;
		newZ = z * 100.f;
		newsX = sX * 100.f;
		newsY = sY * 100.f;
		newsZ = sZ * 100.f;
		newhp = (float)hp / 3000.f;

		if (currSeqX_Count == 16) {
			currSeqX_Count = 0;

		}
		int kind;
		m_Objects[i]->GetKind(&kind);
		if (kind == KIND_HERO)
		{
			m_Renderer->DrawTextureRectHeight(newX, newY, 0, newsX, newsY, r, g, b, a, m_TextIsaac, newZ);
			m_Renderer->DrawSolidRectGauge(newX, newY + newsY, 0, newsY, 3,
				1, 0, 0, 1, newZ, newhp);
		}
		if (kind == KIND_BULLET)
		{
			m_Renderer->DrawTextureRectHeight(newX, newY, 0, newsX, newsY, r, g, b, a, m_TexBullet, newZ);
		}
		if (kind == KIND_LEFT_WALL || kind == KIND_RIGHT_WALL)
		{
			m_Renderer->DrawTextureRectHeight(newX, newY, 0, newsX, newsY, r, g, b, a, m_TexLWall, newZ);
		}
		if (kind == KIND_UP_WALL || kind == KIND_DOWN_WALL)
		{
			m_Renderer->DrawTextureRectHeight(newX, newY, 0, newsX, newsY, r, g, b, a, m_TexUWall, newZ);
		}
		if (kind == KIND_ENEMY)
		{
			m_Renderer->DrawTextureRectHeight(newX, newY, 0, newsX, newsY, r, g, b, a, m_TexEnemy, newZ);
			m_Renderer->DrawSolidRectGauge(newX, newY + newsY, 0, newsY, 3,
				1, 0, 0, 1, newZ, newhp);
		}
	}
	
}

void ScnMgr:: Update(float e_Time)
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;
		m_Objects[i]->Update(e_Time);
	}
}

void ScnMgr::UpdateCollision()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;

		int CollisionCounter = 0;

		for (int j = i + 1; j < MAX_OBJECTS; j++)
		{
			if (m_Objects[j] == NULL)
				continue;
			if (i == j)
			{
				continue;
			}
			float minX, minY, minZ, maxX, maxY, maxZ; //src
			float pX, pY, pZ, sX, sY, sZ;
			m_Objects[i]->GetPos(&pX, &pY, &pZ);
			m_Objects[i]->GetSize(&sX, &sY, &sZ);
			minX = pX - sX / 2.f; maxX = pX + sX / 2.f;
			minY = pY - sY / 2.f; maxY = pY + sY / 2.f;
			minZ = pZ - sZ / 2.f; maxZ = pZ + sZ / 2.f;

			float minX1, minY1, minZ1, maxX1, maxY1, maxZ1; //dist
			m_Objects[j]->GetPos(&pX, &pY, &pZ);
			m_Objects[j]->GetSize(&sX, &sY, &sZ);
			minX1 = pX - sX / 2.f; maxX1 = pX + sX / 2.f;
			minY1 = pY - sY / 2.f; maxY1 = pY + sY / 2.f;
			minZ1 = pZ - sZ / 2.f; maxZ1 = pZ + sZ / 2.f;

			if (BBCollision(minX, minY, minZ, maxX, maxY, maxZ,
				minX1, minY1, minZ1, maxX1, maxY1, maxZ1))
			{
				CollisionCounter++;
				
				std::cout << "Object" << i << "collides" << "Object" << j << std::endl;
				
				ProcessCollision(i, j);
			
			}

		}

		if (CollisionCounter > 0)
		{
			m_Objects[i]->SetColor(1, 0, 0, 1);
		}
		else
		{
			m_Objects[i]->SetColor(1, 1, 1, 1);

		}

	}
}

void ScnMgr::ProcessCollision(int i, int j)
{
	Object * obj1 = m_Objects[i];
	Object * obj2 = m_Objects[j];

	int kindObj1; obj1->GetKind(&kindObj1);
	int kindObj2; obj2->GetKind(&kindObj2);

	if (kindObj1 == KIND_BULLET && kindObj2 == KIND_ENEMY)
	{
		int bulletHP; obj1->GetHP(&bulletHP);
		int enemyHP; obj2->GetHP(&enemyHP);
		int resultEnemyHP = enemyHP - bulletHP;
		int resultBulletHP = 0;

		obj1->SetHP(resultBulletHP);
		obj2->SetHP(resultEnemyHP);
	}

	if (kindObj2 == KIND_BULLET && kindObj1 == KIND_ENEMY)
	{
		int bulletHP; obj2->GetHP(&bulletHP);
		int enemyHP; obj1->GetHP(&enemyHP);
		int resultEnemyHP = enemyHP - bulletHP;
		int resultBulletHP = 0;

		obj2->SetHP(resultBulletHP);
		obj1->SetHP(resultEnemyHP);
	}

	if (kindObj1 == KIND_LEFT_WALL && kindObj2 == KIND_HERO)
	{
		obj2->SetVel(1.5f, 0.f, 0.f);
	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_LEFT_WALL)
	{
		obj1->SetVel(1.5f, 0.f, 0.f);

	}

	if (kindObj1 == KIND_RIGHT_WALL && kindObj2 == KIND_HERO)
	{
		obj2->SetVel(-1.5f, 0.f, 0.f);

	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_RIGHT_WALL)
	{
		obj1->SetVel(-1.5f, 0.f, 0.f);

	}
	
	if (kindObj1 == KIND_UP_WALL && kindObj2 == KIND_HERO)
	{
		obj2->SetVel(0.f, -1.5f, 0.f);

	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_UP_WALL)
	{
		obj1->SetVel(0.f, -1.5f, 0.f);

	}
	
	if (kindObj1 == KIND_DOWN_WALL && kindObj2 == KIND_HERO)
	{
		obj2->SetVel(0.f, 1.5f, 0.f);

	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_DOWN_WALL)
	{
		obj1->SetVel(0.f, 1.5f, 0.f);

	}
	if (kindObj1 == KIND_ENEMY && kindObj2 == KIND_HERO)
	{
		int HeroHp; obj2->GetHP(&HeroHp);
		HeroHp -= 2.f;
		obj2->SetHP(HeroHp);
	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_ENEMY)
	{
		int HeroHp; obj1->GetHP(&HeroHp);
		HeroHp -= 2.f;
		obj1->SetHP(HeroHp);

	}

}

void ScnMgr::AddObject(float x, float y, float z, float V_x, float V_y,float V_z, float S_x, float S_y, float S_z, int kind,
	int hp, int state)
{
	
	int index = FindEmptySlot();
	
	if (index < 0)  
		return;
	
	m_Objects[index] = new Object();   
	m_Objects[index]->SetPos(x, y, z);
	m_Objects[index]->SetVel(V_x, V_y, V_z);
	m_Objects[index]->SetAcc(0.f, 0.f, 0.f);  
	m_Objects[index]->SetMass(1.f);
	m_Objects[index]->SetFrictionCoef(0.5f);
	m_Objects[index]->SetSize(S_x, S_y, S_z);
	m_Objects[index]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Objects[index]->SetKind(kind);
	m_Objects[index]->SetHP(hp);
	m_Objects[index]->SetState(state);
}
void ScnMgr::DeleteObject(unsigned int idx)
{
	if (m_Objects[idx] != NULL)
	{
		delete m_Objects[idx];
		m_Objects[idx] = NULL;			
	}

}

int ScnMgr::FindEmptySlot()		
{
	int index = -1;
	for (int i = 0; i < MAX_OBJECTS; i++) 
	{
		if (m_Objects[i] == NULL) 
		{
			index = i;
			return index;
			std::cout << "슬롯이 비어있음" << std::endl;
		}
	}
	if (index == -1)
	{
		std::cout << "all slots are occupied" << std::endl;


	}
	return index;

}


void ScnMgr::Shoot(int direction, float etime)
{											
	if (direction == SHOOT_NONE)
	{
		return;
	}
	
	if (!m_Objects[HERO_ID]->CanFireBullet())
	{
		return;
	}
	
	float bvX, bvY;
	bvX = 0.f;
	bvY = 0.f;
	float amount = 10.f;
	
	switch (direction)
	{
	
	case SHOOT_UP:
		bvX = 0.f;
		bvY = amount;
		break;
	
	case SHOOT_DOWN:
		bvX = 0.f;
		bvY = -amount;
		break;
	
	case SHOOT_RIGHT:
		bvX = amount;
		bvY = 0.f;
		break;
	
	case SHOOT_LEFT:
		bvX = -amount;
		bvY = 0.f;
		break;

	}
	
	float pX, pY, pZ, vX, vY, vZ;	
	m_Objects[HERO_ID]->GetPos(&pX, &pY, &pZ);		
	m_Objects[HERO_ID]->GetVel(&vX, &vY, &vZ);		
													
	bvX = bvX + vX;
	bvY = bvY + vY;
	
	AddObject(pX, pY, pZ + 0.2f, bvX, bvY, 0.f, 0.2f, 0.2f, 0.5f, KIND_BULLET, 100.f, STATE_AIR);
	float accuTime = 0.f;
	accuTime += etime;
	if (accuTime >= 0.02f) {
		m_sound->PlaySounds(m_soundFiring, true, 1.f);
		accuTime = 0.f;
	}
	m_Objects[HERO_ID]->InitBulletCooltime();
}
/*
bool ScnMgr::RRCollision(float minX, float minY, float maxX, float maxY
	,float minX1, float minY1, float maxX1, float maxY1) 
{
	if (minX > maxX1)
		return false;
	if (maxX < minX1)
		return false;
	if (minY > maxY1)
		return false;
	if (maxY < minY1)
		return false;
	return true;

}
*/

bool ScnMgr::BBCollision(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,
	float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1)
{
	if (minX > maxX1)
		return false;
	if (maxX < minX1)
		return false;
	if (minY > maxY1)
		return false;
	if (maxY < minY1)
		return false;
	if (minZ > maxZ1)
		return false;
	if (maxZ < minZ1)
		return false;
	
	return true;

}

void ScnMgr::Addforce(float x, float y, float z, float etime)
{
	int state;
	m_Objects[HERO_ID]->GetState(&state);
	if (state == STATE_AIR)
	{
		z = 0.f;
	}
	
	m_Objects[HERO_ID]->Addforce(x, y, z, etime);

}

void ScnMgr::DoGarbageCollect()
{
	int hp;
	
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;
	
		int hp = 0;
		m_Objects[i]->GetHP(&hp);
		if (hp == 0)
		{
			DeleteObject(i);
			continue;
		}

		float px, py, pz;
		m_Objects[i]->GetPos(&px, &py, &pz);
		if (px > 5.f || px < -5.f || py > 5.f || py < -5.f || pz <= 0)
		{
			int kind;
			m_Objects[i]->GetKind(&kind);
			if (kind == KIND_BULLET)
			{
				DeleteObject(i);
				continue;
					
			}

		}
		int kind;
		float vx, vy, vz;
		m_Objects[i]->GetVel(&vx, &vy, &vz);
		m_Objects[i]->GetKind(&kind);
		float mag = sqrtf(vx * vx + vy * vy + vz * vz);
		if (mag < FLT_EPSILON && kind == KIND_BULLET)
		{
			DeleteObject(i);
			continue;
		}

	}

}





