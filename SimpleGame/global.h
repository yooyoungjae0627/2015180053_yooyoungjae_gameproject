#pragma once

#define MAX_OBJECTS 30

#define HERO_ID 0		// 히어로는 0번
#define BULLET_ID 1		// 총알은 1번

#define SHOOT_NONE 0
#define SHOOT_UP 1
#define SHOOT_DOWN 2
#define SHOOT_LEFT 3
#define SHOOT_RIGHT 4

#define KIND_HERO 0
#define KIND_BULLET 1
#define KIND_BUILDING 2
#define KIND_LEFT_WALL 3
#define KIND_RIGHT_WALL 4
#define KIND_UP_WALL 5
#define KIND_DOWN_WALL 6
#define KIND_ENEMY 7


#define STATE_AIR 0 //on the sky
#define STATE_GROUND 1 //on the floor

#define GRAVITY 9.8
