#pragma once
class Object
{
private:
	float m_PosX = 0.f;
	float m_PosY = 0.f;
	float m_PosZ = 0.f;
	float m_sX = 0.f;
	float m_sY = 0.f;
	float m_sZ = 0.f;
	float m_R, m_G, m_B, m_A;
	float m_VelX = 0.f;
	float m_VelY = 0.f;
	float m_VelZ = 0.f;
	float m_AccX = 0.f;
	float m_AccY = 0.f;
	float m_AccZ = 0.f;
	float m_Mass = 0.f;
	float m_FrictionCoef = 0.f; 
	float m_HeightTest = 0.f;
	int m_HP = 10000.f;
	int m_kind = 0.f;
	int m_state = 0.f;
	float m_bulletcoolTime = 0.0f;
	float m_bulletTime = 0.0f;

public:
	Object();
	~Object();
	void SetPos(float x, float y, float z);
	void SetSize(float sX, float sY, float sZ);
	void SetColor(float R, float G, float B, float A);
	void SetVel(float x, float y, float z);
	void SetAcc(float x, float y, float z);
	void SetMass(float mass);
	void SetHP(int hp);
	void SetFrictionCoef(float x);
	void SetKind(int kind);
	void SetState(int state);

	void GetPos(float *x, float *y, float *z);
	void GetSize(float *sX, float *sY, float *sZ);
	void GetColor(float *R, float *G, float *B, float *A);
	void GetVel(float *x, float *y, float *z);
	void GetAcc(float *x, float *y, float *z);
	void GetMass(float *mass);
	void GetFrictionCoef(float *x);
	void GetHP(int *hp);
	void GetKind(int *kind);
	void GetState(int *state);
	void Update(float elapsed_Time);
	void Addforce(float _x, float _y, float _z, float _etime);
	
	void InitBulletCooltime();
	bool CanFireBullet();

};

