#pragma once

#include "Object.h"
#include "Renderer.h"
#include "global.h"
#include "Sound.h"

class ScnMgr
{

public:
	ScnMgr(int width, int height);
	~ScnMgr();

private:
	Object *m_Objects[MAX_OBJECTS];
	
	Renderer *m_Renderer = nullptr;

	GLuint m_TextIsaac = 0;
	GLuint m_TexSeq = 0;
	GLuint m_TexBullet = 0;
	GLuint m_TexLWall = 0;
	GLuint m_TexUWall = 0;
	GLuint m_TexEnemy = 0;

	Sound *m_sound;
	
	int m_soundBG = 0;
	int m_soundFiring = 0;
	int m_soundBulletCollision = 0;
	int m_screen_width = 0;
	int m_screen_height = 0;

public:
	void RenderScene();
	void Update(float e_Time);
	void AddObject(float x, float y, float z, float V_x, float V_y, float V_z, float S_x, float S_y, float S_z, int kind, int hp, int state); 
	void DeleteObject(unsigned int idx);
	int FindEmptySlot();
	void Shoot(int direction, float etime);
	void Addforce(float x, float y, float z, float etime);
	void ProcessCollision(int i, int j);

	void UpdateCollision();
	//bool RRCollision(float minX, float minY, float maxX, float maxY,
	//	float minX1, float minY1, float maxX1, float maxY1);

	bool BBCollision(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,
		float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1);

	void DoGarbageCollect();

};
